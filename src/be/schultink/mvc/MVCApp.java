package be.schultink.mvc;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

public class MVCApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = FXMLLoader.load(getClass().getResource("MVC.fxml"));

        stage.setTitle("Model View Controller");
        stage.setScene(scene);
        stage.show();
    }
}
