package be.schultink.mvc;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
//fx:controller="be.schultink.mvc.Controller
public class Controller {
    private Model model = new Model();
    @FXML
    private Label label; //fx:id="label"

    @FXML
    private void buttonPressed(ActionEvent ev) { // #buttonPressed
        model.increment();
        label.setText("Count: " + model.getCount());
    }
    @FXML
    private void resetPressed(ActionEvent ev2) { // #resetPressed
        model.reset();
        label.setText("Count: " + model.getCount());
        }
}
