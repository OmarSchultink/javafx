package be.schultink.mvc;

public class Model {
    private int count;

    public void increment(){
        count++;
    }

    public int getCount() {
        return count;
    }

    public void reset() { count = 0;}
}
