package be.schultink.opdracht5;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

public class Opdracht5 extends Application {
    public static void main(String[] args) {
        launch(args);}

        @Override
        public void start(Stage stage) throws Exception {
            Parent root = FXMLLoader.load(getClass().getResource("Opdracht5.fxml"));
            Scene scene = new Scene(root,800,400);
            stage.setTitle("Pizza");
            stage.setScene(scene);
            stage.show();
        }

}
