package be.schultink.opdracht7;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;


public class MouseEventController {

    @FXML
    private Text text;

    @FXML
    private void mouseMoved(MouseEvent ev){
        text.setText(String.format("Mouse moved (%.0f,%.0f) ",
                ev.getX(), ev.getY()));
    }
    @FXML
    private void mouseClicked(MouseEvent ev){
        text.setText(String.format("Mouse %s clicked (%.0f,%.0f) ",
                ev.getButton().name(), ev.getX(), ev.getY()));
    }
    @FXML
    private void mouseEntered(MouseEvent ev){
        text.setText(String.format("Mouse entered (%.0f,%.0f) ",
                ev.getX(), ev.getY()));

    }
    @FXML
    private void mouseExited (MouseEvent ev){
        text.setText(String.format("Mouse exited (%.0f,%.0f) ",
                ev.getX(), ev.getY()));
    }

}
