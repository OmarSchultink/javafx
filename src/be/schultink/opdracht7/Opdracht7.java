package be.schultink.opdracht7;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Opdracht7 extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Opdracht7.fxml"));
        Scene scene = new Scene(root,600,600);
        stage.setTitle("Event Handling");
        stage.setScene(scene);
        stage.show();
    }



}