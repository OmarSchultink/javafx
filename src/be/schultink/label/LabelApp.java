package be.schultink.label;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;


public class LabelApp extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root =
                FXMLLoader.load(getClass().getResource("Label.fxml"));
        Scene scene = new Scene(root, 300, 150);
        stage.setTitle("Labels");
        stage.setScene(scene);
        stage.show();

    }
}
