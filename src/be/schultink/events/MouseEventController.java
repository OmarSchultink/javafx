package be.schultink.events;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import javafx.scene.input.MouseEvent;


public class MouseEventController {
    @FXML
    private Text text;

    @FXML
    private void mouseMoved(MouseEvent ev){
        text.setText(String.format("Mouse moved (%.0f,%.0f) ",
                ev.getX(), ev.getY()));
    }

}
