package be.schultink.events;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

public class MouseEvent extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("MouseEvent.fxml"));
        Scene scene = new Scene(root, 750, 750);
        stage.setTitle("Mouse events");
        stage.setScene(scene);
        stage.show();
    }
}