package be.schultink.events;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

public class Controller {
    @FXML
    private TextArea textArea;
    @FXML
    private void buttonPressed (ActionEvent ev) {
        textArea.appendText("Button pressed!\n");
    }
    @FXML
    private void clear(ActionEvent ev){
        textArea.clear();
    }
}
