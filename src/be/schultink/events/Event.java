package be.schultink.events;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

public class Event extends Application {
    public static void main(String[] args) {
        launch(args);}

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Event.fxml"));
        Scene scene = new Scene(root,800,400);
        stage.setTitle("Events");
        stage.setScene(scene);
        stage.show();
    }

}
