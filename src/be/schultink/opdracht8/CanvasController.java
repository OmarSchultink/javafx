package be.schultink.opdracht8;


import javafx.scene.canvas.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class CanvasController {
@FXML
    private Canvas canvas;
    private GraphicsContext gc;
    private Image image;
    double x;
    double y;

    @FXML
    public void initialize() {
        gc = canvas.getGraphicsContext2D();
        gc.setStroke(Color.ROYALBLUE);
        gc.setLineWidth(2);
        gc.setFill(Color.BEIGE);
        gc.setFont(Font.font(36));
        image = new Image(getClass().getResourceAsStream("java.png"));
        clear();
    }

    @FXML
    private void rectangle(ActionEvent ev) {
        gc.strokeRect(10, 10, 80, 50);
    }

    @FXML
    private void circle(ActionEvent ev) {
        gc.strokeOval(100, 10, 50, 50);
    }

    @FXML
    private void triangle(ActionEvent ev) {
        double[] yPoints = {10, 10, 60};
        double[] xPoints = {150, 210, 180};
        gc.strokePolygon(xPoints, yPoints, 3);
    }

    @FXML
    private void text(ActionEvent ev) {
        gc.strokeText("My first drawing", 10, 100);
    }
    @FXML
    private void image (ActionEvent ev) {
        gc.drawImage(image, 220 ,10,50,50);
    }
    @FXML
    private void clear (ActionEvent ev) {
        clear();
    }
    private void clear () {
        gc.fillRect(0,0,canvas.getWidth(), canvas.getHeight());
    }
    public void onMouseDragged(MouseEvent ev){
        gc.strokeLine(x,y,ev.getX(), ev.getY());
        x = ev.getX();
        y = ev.getY();
    }
    public void onMousePressed (MouseEvent ev) {
        x = ev.getX();
        y = ev.getY();
    }


}


